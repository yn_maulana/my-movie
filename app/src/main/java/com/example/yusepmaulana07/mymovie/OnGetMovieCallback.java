package com.example.yusepmaulana07.mymovie;

public interface OnGetMovieCallback {

    void onSuccess(Movie movie);

    void onError();
}
