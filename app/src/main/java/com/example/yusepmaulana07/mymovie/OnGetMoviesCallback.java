package com.example.yusepmaulana07.mymovie;

import java.util.List;

public interface OnGetMoviesCallback {

    void onSuccess(int page , List<Movie> movies);

    void onError();
}