package com.example.yusepmaulana07.mymovie;

public interface OnMoviesClickCallback {
    void onClick(Movie movie);
}
